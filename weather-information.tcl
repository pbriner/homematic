        var url = "http://api.wunderground.com/api/f773998d7e021c6c/conditions/lang:DL/q/Switzerland/Zofingen.xml";

		!hier ist die Abfrage mit CUxD
        dom.GetObject("CUxD.CUX2801001:1.CMD_SETS").State("wget -q -O - '"#url#"'");
        dom.GetObject("CUxD.CUX2801001:1.CMD_QUERY_RET").State(1);
        string wetter_xml = dom.GetObject("CUxD.CUX2801001:1.CMD_RETS").State();



        !Beim XML-File den ueberfluessigen Header entfernen
        integer laenge = wetter_xml.Length();
        integer wort_position = wetter_xml.Find("display_location");
        wetter_xml = wetter_xml.Substr(wort_position, (laenge - wort_position));
        !WriteLine(wetter_xml);

        !Daten mit Suchworten aus XML-File ausfiltern:

        !string word = "full";
        string word = "city";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        dom.GetObject("W_Station").State(daten);
		!WriteLine(daten);

        !string word = "observation_time";
        string word = "observation_time_rfc822";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        !daten = daten.Substr(0, (word_position -2));
        daten = daten.Substr(0, (word_position -11));
        dom.GetObject("W_Aktualisierung").State(daten);

        string word = "weather";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        dom.GetObject("W_Bedingungen").State(daten);

        string word = "temp_c";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        real zahl = daten.ToFloat();
        dom.GetObject("W_Temperatur").State(zahl);

        string word = "relative_humidity";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        integer zahl = daten.ToFloat();
        dom.GetObject("W_Luftfeuchte").State(zahl);

        string word = "wind_string";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        dom.GetObject("W_Windbedingungen").State(daten);

        string word = "wind_dir";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        word_laenge =daten.Length();
        string anfangsbuchstabe = daten.Substr(0,1);
        ! Umlaute korrigieren
        ! N # Nord ***
        if (anfangsbuchstabe == "N")
                 { if (daten == "Nordwest") {daten = "Nord-West" ;} }
        ! S # Süd ***
        if (anfangsbuchstabe == "S") {
           ! 4 # SÃ¼d
           if (word_laenge == 4)  {daten = "Süd";}
           ! 8 # SÃ¼dwest
           if (word_laenge == 8)  {daten = "Süd-West";}
           ! 12 # SÃ¼d-SÃ¼dost
           if (word_laenge == 12) {daten = "Süd-Süd-Ost" ;}
             ! 13
           if (word_laenge == 13) {daten = "Süd-Süd-West" ;}   }

        ! W # Westen
        if (anfangsbuchstabe == "W") {
           ! 13 # West-SÃ¼dwest
            if (word_laenge == 13) {daten = "West-Süd-West" ;} }

        ! O # Osten
        if (anfangsbuchstabe == "O") {
           ! 11 # Ost-SÃ¼dost
           if (word_laenge == 11) {daten = "Ost-Süd-Ost" ;} }

        dom.GetObject("W_Windrichtung").State(daten);
        !WriteLine(daten);


        string word = "wind_kph";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        integer zahl = daten.ToFloat();
        dom.GetObject("W_Windgeschwindigkeit").State(zahl);



        string word = "pressure_mb";
        integer word_laenge = word.Length();
        integer word_position = wetter_xml.Find(word);
        string daten = wetter_xml.Substr((word_position + word_laenge +1), 100);
        integer word_position = daten.Find(word);
        daten = daten.Substr(0, (word_position -2));
        integer zahl = daten.ToFloat();
        dom.GetObject("W_Luftdruck").State(zahl);


