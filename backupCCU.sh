# Script to backup Homematic CCU to a local file system
# Copyright (C) 2017 Pascal Briner - All Rights Reserved
# Permission to copy and modify is granted under the MIT license
# Last revised 11/3/2017

ccuUser="Admin"
ccuPass="edu33Pdq1"
ccuUrl="http://192.168.76.5"
ccuBackupPath="/config/cp_security.cgi?action=create_backup&amp;sid="
destPath="/Users/brina/Documents/"
reqLog="/tmp/ccurequest.log"


curl -s -i -D $reqLog $ccuUrl/pages/index.htm?NoAutoLogin=true
sid=$(sed -n -e 's/.*sid=//p' $reqLog)


if [ -z "$sid" ]
then
      echo "\$sid is empty, probably too many sessions or not running"
else
      backupurl="$ccuUrl$ccuBackupPath$sid"
      destFile=homematic-ccu_$(date +%Y-%m-%d_%H-%M).sbk
      curl -L -o $destPath$destFile $ccuUrl/config/cp_security.cgi?action=create_backup&sid=$sid
      #curl $ccuUrl/logout.htm?sid=$sid
      #rm $reqLog
fi
